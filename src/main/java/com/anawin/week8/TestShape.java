package com.anawin.week8;

public class TestShape {
    public static void main(String[] args) {
        Square square1 = new Square ("rect1",10,5);
        System.out.println(square1.getName()+" Width"+" = "+square1.getWidth());
        System.out.println(square1.getName()+" Height"+" = "+square1.getHeight());
        square1.calSquareArea();
        square1.print();
        
        Square square2 = new Square ("rect2",5,3);
        System.out.println(square2.getName()+" Width"+" = "+square2.getWidth());
        System.out.println(square2.getName()+" Height"+" = "+square2.getHeight());
        square2.calSquareArea();
        square2.print();

        Circle circle1 = new Circle("circle1",1);
        System.out.println(circle1.getName()+" radius = "+circle1.getRadius());
        circle1.calCirArea();
        circle1.print();
        Circle circle2 = new Circle("circle2",2);
        System.out.println(circle2.getName()+" radius = "+circle2.getRadius());
        circle2.calCirArea();
        circle2.print();

        Triangle triangle1 = new Triangle(5,5,6);
        System.out.println("triangle1"+" a = "+triangle1.getA() + " b = "+triangle1.getB() + " c = " + triangle1.getC());
        triangle1.calTriArea();
        triangle1.print();
        
        square1.calParimeterASquare();
        square1.printPariSqu();

        square2.calParimeterASquare();
        square2.printPariSqu();

        circle1.calPariCir();
        circle1.printPariCir();

        circle2.calPariCir();
        circle2.printPariCir();

        triangle1.calPariTri();
        triangle1.printPariTri();

    }
}
