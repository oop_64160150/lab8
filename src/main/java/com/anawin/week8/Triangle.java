package com.anawin.week8;

public class Triangle {
    private double a ;
    private double b ;
    private double c ;
    public Triangle(double a , double b , double c ){
        this.a = a;
        this.b = b;
        this.c = c;
    }
    public double calTriArea(){
        double area = 0;
        double s = 0;
        s = (a+b+c)/2;
        area = Math.sqrt(s*(s-a)*(s-b)*(s-c));
        return area;
    }
    public double calPariTri(){
        double pari = 0 ;
        pari = a+b+c;
        return pari;
    }
    public double getA(){
        return a;
    }
    public double getB(){
        return b;
    }
    public double getC(){
        return c;
    }
    public void print(){
        System.out.println("Area of triangle1 = "+calTriArea());
    }
    public void printPariTri(){
        System.out.println("Parimeter of triangle1 = "+calPariTri());
    }
}
