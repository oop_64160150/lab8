package com.anawin.week8;

import java.lang.reflect.Parameter;

public class Square {
    private double width ;
    private double height ;
    private String name;
    public Square(String name,double width,double height){
        this.name = name;
        this.width = width;
        this.height = height;
    }
    public double getWidth(){
        return width;
    }
    public double getHeight(){
        return height;
    }
    public String getName(){
        return name;
    }
    public double calSquareArea(){
        double area = 0;
        area = width*height;
        return area;
    }
    public double calParimeterASquare(){
        double Parameter = 0;
        Parameter = 2*(width+height);
        return Parameter;
    }
    public void print(){
        System.out.println("Area of "+name+" = "+calSquareArea());
    }
    public void printPariSqu(){
        System.out.println("Parimeter of "+name+" = "+calParimeterASquare());
    }
}

