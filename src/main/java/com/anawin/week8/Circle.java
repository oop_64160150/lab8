package com.anawin.week8;

public class Circle {
    private double radius ;
    private String name;
    public Circle(String name,double radius){
        this.name = name;
        this.radius = radius;
    }
    
    public double calCirArea(){
        double area = 0 ;
        area = 3.14*(radius*radius);
        return area;
    }
    public double calPariCir(){
        double pari = 0 ;
        pari = 2*(3.14)*radius;
        return pari;
    }
    public double getRadius(){
        return radius;
    }
    public String getName(){
        return name;
    }
        
    public void print(){
        System.out.println("Area of "+name+" = "+calCirArea());
    }
    public void printPariCir(){
        System.out.println("Parimeter of "+name+" = "+calPariCir());

    }
}