package com.anawin.week8;

public class Map {
    private int width;
    private int height;

    public Map(int width,int height){
        this.width = width;
        this.height = height;
}
public int getWidth() {
    return width;
}
public int getHeight(){
    return height;
}
public void print(){
    for(int i = 0; i<height;i++){
        for(int j = 0;j<width;j++){
            System.out.print("-");
        }
        System.out.println();
}
}
}